import {browser, by, element, protractor} from 'protractor';

describe('diplom-project', () => {

    beforeEach(() => {
        browser.get('/start');
    });

    it('Существует заголовок Управляйте своими решениями', () => {
        expect(element(by.css('p')).getText()).toEqual('Управляйте своими решениями!');
    });

    it('Существуют все необходимые элементы в блоке header', () => {
        element.all(by.css('.main-nav-bar')).then(function (mainNavBar) {
            expect(mainNavBar.length).toBe(1);

            element.all(by.css('nav')).then(function (nav) {
                expect(nav.length).toBe(1);

                element.all(by.css('.navbar-header')).then(function (navHeader) {
                    expect(navHeader.length).toBe(1);

                    element.all(by.css('.navbar-toggler')).then(function (navbarToggler) {
                        expect(navbarToggler.length).toBe(1);
                    });
                });

                element.all(by.css('.navbar-collapse')).then(function (navCollapse) {
                    expect(navCollapse.length).toBe(1);
                });
            });
        });
    });

    it('Существуют все необходимые элементы основного контента страницы', () => {
        element.all(by.css('.content-block')).then(function (contentBlock) {
            expect(contentBlock.length).toBe(1);

            element.all(by.css('.arrow-right-button')).then(function (arrowRightButton) {
                expect(arrowRightButton.length).toBe(1);
            });

            element.all(by.css('.back-to-top')).then(function (backTopButton) {
                expect(backTopButton.length).toBe(1);
            });

            element.all(by.css('.side-nav-bar')).then(function (sideNavBar) {
                expect(sideNavBar.length).toBe(1);
            });


            element.all(by.css('app-home-page')).then(function (appHomePage) {
                expect(appHomePage.length).toBe(1);

                element.all(by.css('.carousel')).then(function (carousel) {
                    expect(carousel.length).toBe(1);

                    element.all(by.css('.btn-yellow')).then(function (btnYellow) {
                        expect(btnYellow.length).toBe(1);
                    });
                });

                element.all(by.css('.block')).then(function (blockItems) {
                    expect(blockItems.length).toBe(3);
                });
            });

        });
    });

    it('Работает переход по кнопке на страницу авторизации ', () => {
        element.all(by.css('.btn-yellow')).then(function (btnYellow) {
            expect(btnYellow.length).toBe(1);

            const btnStart = btnYellow[0];
            btnStart.click();

            expect(element(by.css('.text-header')).getText()).toEqual('Авторизация');
        });
    });
});
