import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {BaseApi} from '../core/base-api';
import {User} from '../models/user';


@Injectable()
export class UserService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  getUserById(id: number): Observable<User> {
    return this.get(`users/${id}`)
      .map((u: User) => u ? u : undefined);
  }

  getUserByEmail(email: string): Observable<User> {
    return this.get(`users/getuserbyemail/${email}`)
        .map((u: User) => u ? u : undefined);
  }

  createNewUser(user: User): Observable<User> {
    return this.post('users', user)
      .map((u: User) => u ? u : undefined);
  }
}
