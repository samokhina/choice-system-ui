import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {StartComponent} from './start.component';
import {HomePageComponent} from './home-page/home-page.component';
import {TechnologyPageComponent} from './technology-page/technology-page.component';
import {SolutionsPageComponent} from './solutions-page/solutions-page.component';
import {ContactsPageComponent} from './contacts-page/contacts-page.component';

const routes: Routes = [
  {
    path: 'start', component: StartComponent, children: [
      {path: '', component: HomePageComponent},
      {path: 'technology', component: TechnologyPageComponent},
      {path: 'solutions', component: SolutionsPageComponent},
      {path: 'contacts', component: ContactsPageComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class StartRoutingModule {

}
