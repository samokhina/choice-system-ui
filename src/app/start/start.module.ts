import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {StartRoutingModule} from './start-routing.module';
import {SharedModule} from '../shared/shared.module';

import {StartComponent} from './start.component';
import { HomePageComponent } from './home-page/home-page.component';
import { TechnologyPageComponent } from './technology-page/technology-page.component';
import { SolutionsPageComponent } from './solutions-page/solutions-page.component';
import { ContactsPageComponent } from './contacts-page/contacts-page.component';





@NgModule({
  declarations: [
    StartComponent,
    HomePageComponent,
    TechnologyPageComponent,
    ContactsPageComponent,
    SolutionsPageComponent,
  ],
  imports: [
    CommonModule,

    StartRoutingModule,
    SharedModule
  ]
})

export class StartModule {

}
