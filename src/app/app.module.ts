import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';


import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';

import {AuthModule} from './auth/auth.module';
import {SystemModule} from './system/system.module';

import {UserService} from './shared/services/user.service';
import {AuthService} from './shared/services/auth.service';
import {StartModule} from './start/start.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'universal-demo-v5'}),
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,

        StartModule,
        AuthModule,
        SystemModule
    ],
    providers: [
        UserService,
        AuthService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
