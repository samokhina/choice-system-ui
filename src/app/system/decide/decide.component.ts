import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Chart} from 'chart.js';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/mergeMap';

import {MessageComponent} from '../../auth/shared/components/message/message.component';
import {DecideService} from '../shared/services/decide.service';

declare let $: any;


interface Intervals {
  a: number;
  b: number;
  c: number;
  d: number;

  segments: {
    k: number;
    m: number;
  } [];
}

interface PairDes {
  x: number;
  y: number;
}


@Component({
  selector: 'app-decide',
  templateUrl: './decide.component.html',
  styleUrls: ['./decide.component.scss']
})


export class DecideComponent implements OnInit {

  showMessage; // 1-> обычный режим  2-> режим модального окна (для вывода сообщения об ошибке)
  @ViewChild(MessageComponent) message: MessageComponent;

  isTypeTask;
  typeTask;
  currentTypeTask;

  // для изменения
  currentTypeDemand;
  currentTypeModifier;

  // итоговые значения
  demands: number[];
  modifiers: number[];


  form: FormGroup;
  formModal: FormGroup;

  valuesX: number[];
  valuesY: number[];
  valuesYForInit;

  start;
  end;
  countStep;

  chart1: Chart;
  chart2: Chart;
  chart3: Chart;


  // изменяем
  indexCurrentDataset;
  currentDatasets: any[];  // изменяем в модальном окне (при закрытии datasets не изменится)

  datasets: any[];  // итоговый

  a;
  d;
  b;
  c;

  aInit;
  bInit;
  cInit;
  dInit;

  mx: number[];  // множество минимальных степеней приналежности (для соотв. X)
  intervals: Intervals[];
  pairs: PairDes[];

  result;


  constructor(private decideService: DecideService,
              private router: Router) {
  }

  ngOnInit() {
    this.showMessage = 1;

    this.isTypeTask = false;
    this.typeTask = -1;

    this.demands = [];
    this.modifiers = [];

    this.currentDatasets = [];
    this.datasets = [];

    this.mx = [];
    this.intervals = [];
    this.pairs = [];

    this.valuesX = [];
    this.valuesY = [];

    this.linksHide();
    this.initForm();

    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;

    const ctx1 = document.getElementById('canvas1');
    this.chart1 = new Chart(ctx1, {
      type: 'line',
      data: {},
      options: {}
    });
  }

  initChart() {
    if (this.form.get('start')['errors'] === null &&
      this.form.get('end')['errors'] === null &&
      this.form.get('step')['errors'] === null) {

      this.demands = [];
      this.modifiers = [];

      this.currentDatasets = [];
      this.datasets = [];

      this.mx = [];
      this.intervals = [];
      this.pairs = [];

      this.valuesX = [];
      this.valuesY = [];

      const step = +((this.end - this.start) / this.countStep).toFixed(1);

      // формируем значения X
      let x = +this.start;
      this.valuesX.push(x);
      for (let i = 1; i < this.countStep - 1; i++) {
        x = +(x + step).toFixed(1);
        this.valuesX.push(x);
      }
      x = +(x + step).toFixed(1);
      if (x < +this.end) {
        this.valuesX.push(x);
      }
      this.valuesX.push(+this.end);


      // формируем значения Y
      const middle = Math.round((this.valuesX.length - 1) / 2);
      this.a = this.valuesX[1];
      this.b = this.valuesX[middle];
      this.c = this.valuesX[middle];
      this.d = this.valuesX[this.valuesX.length - 2];
      this.valuesY = [];
      this.getValuesY(this.valuesX, this.valuesY);

      // для создания нового требования -> по умолчанию
      this.aInit = this.a;
      this.bInit = this.b;
      this.cInit = this.c;
      this.dInit = this.d;

      this.valuesYForInit = this.valuesY;

      const dataset = {
        label: 'Новое требование',
        data: this.valuesYForInit,
        backgroundColor: 'rgba(100, 162, 100, 0.5)',
        fill: true
      };

      this.datasets[0] = dataset;
      this.saveData(0);
      this.indexCurrentDataset = 0;

      const dataChart = {
        labels: this.valuesX,
        datasets: this.datasets
      };

      const optionsChart = {
        legend: {
          display: true
        }
      };

      // по умолчанию
      this.demands[0] = 1;
      this.currentTypeDemand = 1;

      this.modifiers[0] = 0;
      this.currentTypeModifier = 0;


      this.chart1.data = dataChart;
      this.chart1.options = optionsChart;
      this.chart1.update();


      // подготавливаем для страницы редактирования требований
      const ctx = document.getElementById('canvas2');
      this.chart2 = new Chart(ctx, {
        type: 'line',
        data: dataChart,
        options: optionsChart
      });

      const ctx3 = document.getElementById('canvas3');

      this.chart3 = new Chart(ctx3, {
        type: 'line',
        data: dataChart,
        options: optionsChart
      });

    } else {
      this.message.showMessage({
        text: 'Поля содержат ошибки',
        type: 'danger'
      });
    }
  }


  addChart() {
    const dataset = {
      label: 'Новое требование',
      data: this.valuesYForInit,
      backgroundColor: 'rgba(100, 162, 100, 0.5)',
      fill: true
    };

    const i = this.datasets.length;

    this.datasets[i] = dataset;

    this.formModal.get('a').setValue(this.aInit);
    this.formModal.get('b').setValue(this.bInit);
    this.formModal.get('c').setValue(this.cInit);
    this.formModal.get('d').setValue(this.dInit);

    this.saveData(i);

    this.demands[i] = 1;
    this.modifiers[i] = 0;

    this.currentTypeDemand = this.demands[0];
    this.currentTypeModifier = this.modifiers[0];

    this.indexCurrentDataset = 0;

    this.chart2.config.data.datasets = this.datasets;
    this.chart2.update();
  }


  deleteChart() {
    if (this.datasets.length > 0) {

      const i = this.indexCurrentDataset;

      this.datasets.splice(i, 1);
      this.intervals.splice(i, 1);

      this.demands.splice(i, 1);
      this.modifiers.splice(i, 1);

      this.currentTypeDemand = this.demands[0];
      this.currentTypeModifier = this.modifiers[0];

      this.indexCurrentDataset = 0;

      this.chart2.config.data.datasets = this.datasets;
      this.chart2.update();
    } else {
      this.message.showMessage({
        text: 'Больше нет графиков для удаления',
        type: 'danger'
      });
    }
  }

  updateChart() {
    if (this.datasets.length > 0) {

      this.showMessage = 2;
      this.currentDatasets = this.datasets.slice();

      const i = this.indexCurrentDataset;
      this.formModal.get('demand').setValue(this.currentDatasets[i].label);

      this.formModal.get('a').setValue(this.intervals[i].a);
      this.formModal.get('b').setValue(this.intervals[i].b);
      this.formModal.get('c').setValue(this.intervals[i].c);
      this.formModal.get('d').setValue(this.intervals[i].d);

      $('#modal1').show();
      $('#modal2').hide();

      $('#buttonBack').hide();
      $('#buttonNext').show();
      $('#buttonSave').hide();

      $('#modal').modal('show');

    } else {
      this.message.showMessage({
        text: 'Больше нет графиков для изменения',
        type: 'danger'
      });
    }
  }


  initChart3() {
    const i = this.indexCurrentDataset;

    this.valuesY = [];
    this.getValuesY(this.valuesX, this.valuesY);

    const dataset = {
      label: this.formModal.get('demand').value,
      data: this.valuesY,
      backgroundColor: 'rgba(100, 162, 100, 0.5)',
      fill: true
    };

    this.currentDatasets[i] = dataset;

    this.chart3.config.data.datasets = this.currentDatasets;
    this.chart3.update();
  }

  updateData() {
    if (this.a !== '' || this.b !== '' || this.c !== '' || this.d !== '') {

      const i = this.indexCurrentDataset;

      this.valuesY = [];
      this.getValuesY(this.valuesX, this.valuesY);

      const dataset = {
        label: this.formModal.get('demand').value,
        data: this.valuesY,
        backgroundColor: 'rgba(100, 162, 100, 0.5)',
        fill: true
      };

      this.currentDatasets[i] = dataset;

      this.chart3.config.data.datasets = this.currentDatasets;
      this.chart3.update();
    }
  }

  saveChart() {
    if (this.formModal.valid) {
      if (this.valuesX.indexOf(+this.formModal.get('a').value) !== -1 &&
        this.valuesX.indexOf(+this.formModal.get('b').value) !== -1 &&
        this.valuesX.indexOf(+this.formModal.get('c').value) !== -1 &&
        this.valuesX.indexOf(+this.formModal.get('d').value) !== -1) {

        this.showMessage = 1;
        const i = this.indexCurrentDataset;

        this.demands[i] = this.currentTypeDemand;
        this.modifiers[i] = this.currentTypeModifier;

        this.datasets = this.currentDatasets;
        this.chart2.config.data.datasets = this.datasets;
        this.chart2.update();

        $('#modal').modal('hide');

        this.saveData(i);

      } else {
        this.message.showMessage({
          text: 'Введены значения не из интервала',
          type: 'danger'
        });
      }
    } else {
      this.message.showMessage({
        text: 'Поля содержат ошибки',
        type: 'danger'
      });
    }
  }


  getValuesY(valX, valY) {
    let x;
    for (let i = 0; i < valX.length; i++) {
      x = valX[i];

      if (x < this.a || x > this.d) {
        valY.push(null);
      }

      if (x >= this.a && x < this.b) {
        if (this.currentTypeModifier === 1) {
          let val = Math.pow((x - this.a) / (this.b - this.a), 2);
          valY.push(val);
        } else {
          if (this.currentTypeModifier === 2) {
            let val = Math.pow((x - this.a) / (this.b - this.a), 1 / 2);
            valY.push(val);
          } else {
            valY.push((x - this.a) / (this.b - this.a));
          }
        }
      }

      if (x >= this.b && x <= this.c) {
        valY.push(1);
      }

      if (x > this.c && x <= this.d) {
        if (this.currentTypeModifier === 1) {
          let val = Math.pow((this.d - x) / (this.d - this.c), 2);
          valY.push(val);
        } else {
          if (this.currentTypeModifier === 2) {
            let val = Math.pow((this.d - x) / (this.d - this.c), 1 / 2);
            valY.push(val);
          } else {
            valY.push((this.d - x) / (this.d - this.c));
          }
        }
      }
    }
  }


  setCorrentItem(i) {
    this.currentTypeDemand = this.demands[i];
    this.currentTypeModifier = this.modifiers[i];

    this.indexCurrentDataset = i;
  }


  isErrorNext(current) {
    if (current === 1) {
      if (this.form.get('task')['errors'] === null) {
        this.activateTab(current, 2);
      } else {
        if (this.form.get('task')['errors']['required']) {
          this.message.showMessage({
            text: 'Задача не сформулирована',
            type: 'danger'
          });
        }
      }
    }

    if (current === 2) {
      if (this.typeTask === -1) {
        this.message.showMessage({
          text: 'Тип задачи не выбран',
          type: 'danger'
        });
      } else {
        if (this.isTypeTask || this.currentTypeTask !== this.typeTask) {
          if (this.currentTypeTask === 1) {
            $('#link4').hide();
          }
          if (this.currentTypeTask === 2) {
            $('#link3').hide();
            $('#link5').hide();
          }
        }

        this.isTypeTask = true;
        this.currentTypeTask = this.typeTask;
        if (this.typeTask === 1) {
          this.activateTab(current, 4);
        }
        if (this.typeTask === 2) {
          this.activateTab(current, 3);
        }
      }
    }

    if (current === 3) {
      if (this.form.get('start')['errors'] === null &&
        this.form.get('end')['errors'] === null &&
        this.form.get('step')['errors'] === null) {
        this.activateTab(current, 5);
      } else {
        this.message.showMessage({
          text: 'График не инициализован',
          type: 'danger'
        });
      }
    }

    if (current === 5) {
      if (this.datasets.length > 1) {
        this.activateTab(current, 6);

        this.findDecide();

        // console.log('datasets: ', this.datasets);
        // console.log('demands: ', this.demands);
        // console.log('modifiers: ', this.modifiers);

      } else {
        this.message.showMessage({
          text: 'Требований должно быть больше одного',
          type: 'danger'
        });
      }
    }
  }


  isErrorNextModal() {

    $('#modal2').show();
    $('#modal1').hide();

    $('#buttonBack').show();
    $('#buttonNext').hide();
    $('#buttonSave').show();

    this.initChart3();

    if (this.formModal.get('demand')['errors'] === null && this.currentTypeDemand !== -1 && this.currentTypeModifier !== -1) {
    } else {
      if ((this.formModal.get('demand')['errors'] !== null && this.currentTypeDemand === -1 && this.currentTypeModifier === -1) ||
        (this.formModal.get('demand')['errors'] !== null && this.currentTypeDemand === -1 && this.currentTypeModifier !== -1) ||
        (this.formModal.get('demand')['errors'] !== null && this.currentTypeDemand !== -1 && this.currentTypeModifier === -1) ||
        (this.formModal.get('demand')['errors'] === null && this.currentTypeDemand === -1 && this.currentTypeModifier === -1)) {
        this.message.showMessage({
          text: 'Не все форма заполнена',
          type: 'danger'
        });
      } else {

        if (this.currentTypeDemand === -1) {
          this.message.showMessage({
            text: 'Не выбран тип требования',
            type: 'danger'
          });
        } else {
          if (this.currentTypeModifier === -1) {
            this.message.showMessage({
              text: 'Не выбран модификатор',
              type: 'danger'
            });
          } else {
            if (this.formModal.get('demand')['errors']['required']) {
              this.message.showMessage({
                text: 'Требование не сформулировано',
                type: 'danger'
              });
            }
          }
        }
      }
    }
  }


  initForm() {
    this.form = new FormGroup({
      'task': new FormControl(
        '',
        [Validators.required]),

      'start': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$')]),
      'end': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isLess, this.isZero]),
      'step': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isZero])
    });


    this.formModal = new FormGroup({
      'demand': new FormControl(
        null,
        [Validators.required]),

      'a': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isCorrectA]),

      'b': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isCorrectB]),

      'c': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isCorrectC]),

      'd': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$'), this.isCorrectD, this.isZero])

    });
  }


  saveData(index) {
    let z1, z2;
    z1 = this.b - this.a;
    z2 = this.d - this.c;

    this.intervals[index] = {
      a: +this.a,
      b: +this.b,
      c: +this.c,
      d: +this.d,

      segments: [{
        k: 1 / z1,
        m: -(this.a / z1)
      },
        {
          k: -(1 / z2),
          m: this.d / z2
        }]
    };
  }

  findDecide() {

    // ПОИСК ОБЛАСТИ ВОЗМОЖНЫХ РЕШЕНИЙ

    // находим MAX для левого края
    let j, i, ik, jk, max, min, iMax, iMin;

    let datasets;
    let dataX, dataY;

    min = 0;
    dataX = this.chart2.data.labels; // X
    datasets = this.chart2.data.datasets; // Y[]

    for (i = 0; i < datasets.length; i++) {
      j = 0;
      dataY = datasets[i].data;

      while (j < dataY.length && dataY[j] === null) {
        j++;
      }

      if (dataX[j] > min) {
        min = dataX[j];
        iMax = j;
      }
    }

    // находим MIN для правого края
    max = Number.MAX_SAFE_INTEGER;

    for (i = 0; i < datasets.length; i++) {

      dataY = datasets[i].data;
      j = dataY.length - 1;
      while (j > 0 && dataY[j] === null) {
        j--;
      }

      if (dataX[j] < max) {
        max = dataX[j];
        iMin = j;
      }
    }

    // ПОИСК ПЕРЕСЕЧЕНИЙ (РЕШЕНИЙ)

    let x, y;

    for (i = 0; i < this.intervals.length; i++) {
      for (j = i + 1; j < this.intervals.length; j++) {
        for (ik = 0; ik < 2; ik++) {
          for (jk = 0; jk < 2; jk++) {

            const k1 = this.intervals[i].segments[ik].k;
            const k2 = this.intervals[j].segments[jk].k;
            const m1 = this.intervals[i].segments[ik].m;
            const m2 = this.intervals[j].segments[jk].m;

            let z;
            z = k1 - k2;

            if (z !== 0 && isFinite(z) && isFinite(m1) && isFinite(m2)) {

              let x1;
              let x2;
              let discriminant;

              if (this.modifiers[i] === 1 || this.modifiers[j] === 1) {

                if (this.modifiers[i] === 1) {
                  discriminant = Math.pow(2 * k1 * m1 - k2, 2) - 4 * Math.pow(k1, 2) * (Math.pow(m1, 2) - m2);

                  x1 = ((k2 - 2 * k1 * m1) + Math.sqrt(discriminant)) / (2 * Math.pow(k1, 2));
                  x2 = ((k2 - 2 * k1 * m1) - Math.sqrt(discriminant)) / (2 * Math.pow(k1, 2));

                  if (x1 >= min && x1 <= max) {
                    x = x1;
                  } else {
                    x = x2;
                  }
                  y = k1 * x + m1;
                } else {
                  discriminant = Math.pow(2 * k2 * m2 - k1, 2) - 4 * Math.pow(k2, 2) * (Math.pow(m2, 2) - m1);

                  x1 = ((k1 - 2 * k2 * m2) + Math.sqrt(discriminant)) / (2 * Math.pow(k2, 2));
                  x2 = ((k1 - 2 * k2 * m2) - Math.sqrt(discriminant)) / (2 * Math.pow(k2, 2));

                  if (x1 >= min && x1 <= max) {
                    x = x1;
                  } else {
                    x = x2;
                  }
                  y = k2 * x + m2;
                }
              } else {
                if (this.modifiers[i] === 2 || this.modifiers[j] === 2) {

                  if (this.modifiers[i] === 2) {
                    discriminant = Math.pow(2 * k2 * m2 - k1, 2) - 4 * Math.pow(k2, 2) * (Math.pow(m2, 2) - m1);

                    x1 = ((k1 - 2 * k2 * m2) + Math.sqrt(discriminant)) / (2 * Math.pow(k2, 2));
                    x2 = ((k1 - 2 * k2 * m2) - Math.sqrt(discriminant)) / (2 * Math.pow(k2, 2));

                    if (x1 >= min && x1 <= max) {
                      x = x1;
                    } else {
                      x = x2;
                    }
                    y = k2 * x + m2;
                  } else {
                    discriminant = Math.pow(2 * k1 * m1 - k2, 2) - 4 * Math.pow(k1, 2) * (Math.pow(m1, 2) - m2);

                    x1 = ((k2 - 2 * k1 * m1) + Math.sqrt(discriminant)) / (2 * Math.pow(k1, 2));
                    x2 = ((k2 - 2 * k1 * m1) - Math.sqrt(discriminant)) / (2 * Math.pow(k1, 2));

                    if (x1 >= min && x1 <= max) {
                      x = x1;
                    } else {
                      x = x2;
                    }
                    y = k1 * x + m1;
                  }
                } else {
                  x = (m2 - m1) / z;
                  y = k1 * x + m1;
                }
              }

              if (x >= min && x <= max && y >= 0 && y <= 1) {
                this.pairs.push({
                  x: x,
                  y: y
                });
              }
            }
          }
        }
      }
    }

    // ПОИСК МИНИМАЛЬНЫХ СТЕПЕНЕЙ ПРИНАДЛЕЖНОСТИ (ДЛЯ КАЖДОГО X)

    let st, minSt;
    let a, b, c, d;

    for (i = 0; i < this.pairs.length; i++) {

      st = Number.MAX_SAFE_INTEGER;
      minSt = Number.MAX_SAFE_INTEGER;
      x = this.pairs[i].x;

      for (j = 0; j < this.intervals.length; j++) {

        a = this.intervals[j].a;
        b = this.intervals[j].b;
        c = this.intervals[j].c;
        d = this.intervals[j].d;

        if (x >= a && x < b) {
          if (this.modifiers[j] === 1) {
            st = Math.pow((x - a) / (b - a), 2);
          } else {
            if (this.modifiers[j] === 2) {
              st = Math.pow((x - a) / (b - a), 1 / 2);
            } else {
              st = (x - a) / (b - a);
            }
          }
        }

        if (x > c && x <= d) {
          if (this.modifiers[j] === 1) {
            st = Math.pow((d - x) / (d - c), 2);
          } else {
            if (this.modifiers[j] === 2) {
              st = Math.pow((d - x) / (d - c), 1 / 2);
            } else {
              st = (d - x) / (d - c);
            }
          }
        }

        if (st < minSt) {
          minSt = st;
        }
      }
      this.mx.push(minSt);
    }

    // ПОИСК МАКСИМАЛЬНОЙ СТЕПЕНИ ПРИНАДЛЕЖНОСТИ -
    // И СООТВ. ЗНАЧЕНИЯ X - РЕШЕНИЯ

    let findX;
    let maxSt, iMaxSt;
    maxSt = 0;
    for (i = 0; i < this.pairs.length; i++) {
      if (maxSt < this.mx[i]) {
        maxSt = this.mx[i];
        iMaxSt = i;
      }
    }

    findX = this.pairs[iMaxSt].x;
    this.result = findX.toFixed(2);

    console.log('pairs: ', this.pairs);
    console.log('intervals: ', this.intervals);
    console.log('modifiers: ', this.modifiers);
  }


  saveResult() {
    const user = JSON.parse(window.localStorage.getItem('user'));

    const createIntervalSolution$ = this.decideService.createIntervalSolution({
      formulation: this.form.value.task,
      result: this.result,
      type: 'line',
      userId: user.id
    });

    const demands$ = this.decideService.getDemandTypes();
    const modifiers$ = this.decideService.getModifierTypes();

    const createDataChart$ = Observable.combineLatest(createIntervalSolution$, demands$, modifiers$)
      .flatMap(([intervalSolution, demands, modifiers]) => {


        let data$ = [];

        for (let i = 0; i < this.datasets.length; i++) {

          let di, mi;
          let createDataChart;

          for (di = 0; di < demands.length; di++) {
            if ((this.demands[i] === 1 && demands[di].title === 'Ограничение') ||
              (this.demands[i] === 2 && demands[di].title === 'Цель')) {
              break;
            }
          }

          for (mi = 0; mi < demands.length; mi++) {
            if ((this.modifiers[i] === 0 && modifiers[mi].title === '-') ||
              (this.modifiers[i] === 1 && modifiers[mi].title === 'Очень') ||
              (this.modifiers[i] === 2 && modifiers[mi].title === 'Почти')) {
              break;
            }
          }

          createDataChart = {
            label: this.datasets[i].label,
            color: this.datasets[i].backgroundColor,

            chartId: intervalSolution.id,
            demandTypeId: demands[di].id,
            modifierTypeId: modifiers[mi].id
          }
          data$[i] = this.decideService.createDataChart(createDataChart);
        }
        return Observable.combineLatest(data$);
      });

    const createValues$ = createDataChart$
      .flatMap((dataCharts) => {

        let index;
        index = 0;
        let data$ = [];
        for (let i = 0; i < this.datasets.length; i++) {

          let createValue;
          for (let j = 0; j < this.valuesX.length; j++) {

            let y;
            if (this.datasets[i].data[j] === null) {
              y = -1;
            } else {
              y = this.datasets[i].data[j];
            }

            createValue = {
              x: this.valuesX[j],
              y: y,
              dataChartId: dataCharts[i].id
            }
            data$[index] = this.decideService.createValue(createValue);
            index++;
          }
        }
        return Observable.combineLatest(data$);
      });

    createValues$
      .subscribe(() => {
          this.router.navigate(['/system']);
        }
      );
  }

  onTypeButton(typeButton, typeNumber) {

    if (typeButton === 'task') {
      this.typeTask = typeNumber;
    }

    if (typeButton === 'demand') {
      this.currentTypeDemand = typeNumber;
    }

    if (typeButton === 'modifier') {
      this.currentTypeModifier = typeNumber;
    }
  }


  activateTab(current, next) {

    $('#link' + next).show();
    $('#tab' + current).removeClass(['active', 'show']);
    $('#tab' + next).addClass(['active', 'show']);
    $('#link' + current).removeClass(['active']);
    $('#link' + next).addClass(['active']);
  }

  linksHide() {
    $('#link2').hide();
    $('#link3').hide();
    $('#link4').hide();
    $('#link5').hide();
    $('#link6').hide();
  }

  isErrorBackModal() {
    $('#modal2').hide();
    $('#modal1').show();

    $('#buttonBack').hide();
    $('#buttonNext').show();
    $('#buttonSave').hide();
  }


  isLess(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.parent !== undefined) {
      if (control.value !== undefined && control.parent.controls['start'].value !== undefined) {
        if (control.value <= control.parent.controls['start'].value) {
          return {'less': true};
        }
      }
    }
    return null;
  }

  isZero(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.value !== undefined) {
      if (control.value === '0') {
        return {'zero': true};
      }
    }
    return null;
  }

  isCorrectA(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.parent !== undefined) {

      const val = control.value;
      const valB = control.parent.controls['b'].value;

      const reg = new RegExp('^[0-9]*[.,]?[0-9]+$');

      if (val !== undefined && reg.test(val) && valB !== undefined && reg.test(valB)) {
        if (val > valB) {
          return {'moreB': true};
        }

        if (val <= valB && control.parent.controls['b']['errors'] !== null) {
          if ('lessA' in control.parent.controls['b']['errors']) {
            control.parent.controls['b']['errors']['lessA'] = null;

            if (!('required' in control.parent.controls['b']['errors']) &&
              !('pattern' in control.parent.controls['b']['errors'])) {
              control.parent.controls['b']['errors'] = null;
              control.parent.controls['b']['status'] = 'valid';
            }
          }
        }
      }
    }
    return null;
  }

  isCorrectD(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.parent !== undefined) {

      const val = control.value;
      const valC = control.parent.controls['c'].value;

      const reg = new RegExp('^[0-9]*[.,]?[0-9]+$');

      if (val !== undefined && reg.test(val) && valC !== undefined && reg.test(valC)) {
        if (val < valC) {
          return {'lessC': true};
        }

        if (val >= valC && control.parent.controls['c']['errors'] !== null) {
          if ('moreD' in control.parent.controls['c']['errors']) {
            control.parent.controls['c']['errors']['moreD'] = null;

            if (!('required' in control.parent.controls['c']['errors']) &&
              !('pattern' in control.parent.controls['c']['errors']) &&
              !('zero' in control.parent.controls['c']['errors'])) {
              control.parent.controls['c']['errors'] = null;
              control.parent.controls['c']['status'] = 'valid';
            }
          }
        }
      }
    }
    return null;
  }

  isCorrectB(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.parent !== undefined) {
      const val = control.value;
      const valA = control.parent.controls['a'].value;
      const valC = control.parent.controls['c'].value;

      const reg = new RegExp('^[0-9]*[.,]?[0-9]+$');

      if (val !== undefined && reg.test(val)) {

        if (valC !== undefined && reg.test(valC)) {
          if (val > valC) {
            return {'moreC': true};
          }
          if (val <= valC && control.parent.controls['c']['errors'] !== null) {
            if ('lessB' in control.parent.controls['c']['errors']) {
              control.parent.controls['c']['errors']['lessB'] = null;

              if (!('required' in control.parent.controls['c']['errors']) &&
                !('pattern' in control.parent.controls['c']['errors'])) {
                control.parent.controls['c']['errors'] = null;
                control.parent.controls['c']['status'] = 'valid';
              }
            }
          }
        }

        if (valA !== undefined && reg.test(valA)) {
          if (val < valA) {
            return {'lessA': true};
          }
          if (val >= valA && control.parent.controls['a']['errors'] !== null) {
            if ('moreB' in control.parent.controls['a']['errors']) {
              control.parent.controls['a']['errors']['moreB'] = null;

              if (!('required' in control.parent.controls['a']['errors']) &&
                !('pattern' in control.parent.controls['a']['errors'])) {
                control.parent.controls['a']['errors'] = null;
                control.parent.controls['a']['status'] = 'valid';
              }
            }
          }
        }
      }
    }
    return null;
  }

  isCorrectC(control: FormControl): {
    [s: string
      ]: boolean
  } {
    if (control.parent !== undefined) {

      const val = control.value;

      const valD = control.parent.controls['d'].value;
      const valB = control.parent.controls['b'].value;

      const reg = new RegExp('^[0-9]*[.,]?[0-9]+$');

      if (val !== undefined && reg.test(val)) {
        if (valD !== undefined && reg.test(valD)) {

          if (val > valD) {
            return {'moreD': true};
          }

          if (val <= valD && control.parent.controls['d']['errors'] !== null) {
            if ('lessC' in control.parent.controls['d']['errors']) {
              control.parent.controls['d']['errors']['lessC'] = null;

              if (!('required' in control.parent.controls['d']['errors']) &&
                !('pattern' in control.parent.controls['d']['errors'])) {
                control.parent.controls['d']['errors'] = null;
                control.parent.controls['d']['status'] = 'valid';
              }
            }
          }
        }

        if (valB !== undefined && reg.test(valB)) {

          if (val < valB) {
            return {'lessB': true};
          }

          if (val >= valB && control.parent.controls['b']['errors'] !== null) {
            if ('moreC' in control.parent.controls['b']['errors']) {
              control.parent.controls['b']['errors']['moreC'] = null;

              if (!('required' in control.parent.controls['b']['errors']) &&
                !('pattern' in control.parent.controls['b']['errors'])) {
                control.parent.controls['b']['errors'] = null;
                control.parent.controls['b']['status'] = 'valid';
              }
            }
          }
        }
      }
    }
    return null;
  }
}
