import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedModule} from '../shared/shared.module';
import {SystemRoutingModule} from './system-rouiting.module';
import {AuthModule} from '../auth/auth.module';

import {SystemComponent} from './system.component';
import {HelpComponent} from './help/help.component';
import {DecideComponent} from './decide/decide.component';
import {SolutionsComponent} from './solutions/solutions.component';
import {SolutionComponent} from './solutions/solution/solution.component';

import {ShowtabDirective} from './shared/directives/showtab.directive';

import {DecideService} from './shared/services/decide.service';
import {HowToMakeDecisionComponent} from './help/how-to-make-decision/how-to-make-decision.component';
import { HowToViewSolutionsComponent } from './help/how-to-view-solutions/how-to-view-solutions.component';
import { HowToManageSolutionsComponent } from './help/how-to-manage-solutions/how-to-manage-solutions.component';


@NgModule({
    declarations: [
        SystemComponent,
        DecideComponent,
        SolutionsComponent,
        SolutionComponent,
        HelpComponent,
        HowToMakeDecisionComponent,
        HowToManageSolutionsComponent,
        HowToViewSolutionsComponent,

        ShowtabDirective,

        HowToMakeDecisionComponent,

        HowToViewSolutionsComponent,

        HowToManageSolutionsComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        SystemRoutingModule,
        AuthModule
    ],
    providers: [
        DecideService
    ]
})

export class SystemModule {

}
