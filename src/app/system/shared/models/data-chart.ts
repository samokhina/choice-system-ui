import {Chart} from './chart';
import {DemandType} from './demand-type';
import {ModifierType} from './modifier-type';

export class DataChart {
    constructor(public label: string,
                public color: string,
                public chart: Chart,
                public demandType: DemandType,
                public modifierType: ModifierType,
                public id?: number) {
    }
}
