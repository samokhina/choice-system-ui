import {IntervalSolution} from './interval-solution';

export class Chart {
    constructor(public type: string,
                public intervalSolution: IntervalSolution,
                public id?: number) {
    }
}
