import {DataChart} from './data-chart';

export class Value {
    constructor(public x: number,
                public y: number,
                public dataChart: DataChart,
                public id?: number) {
    }
}
