import {User} from '../../../shared/models/user';

export class IntervalSolution {
    constructor(public formulation: string,
                public result: number,
                public user: User,
                public id?: number) {
    }
}
