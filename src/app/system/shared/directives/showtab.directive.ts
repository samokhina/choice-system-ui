import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appShowTab]'
})

export class ShowtabDirective {
  @HostBinding('class.active') isOpen = false;

  @HostListener('click') onClick() {
    this.isOpen = !this.isOpen;
  }
}
