import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';

import {BaseApi} from '../../../shared/core/base-api';

import {DemandType} from '../models/demand-type';
import {ModifierType} from '../models/modifier-type';
import {IntervalSolution} from '../models/interval-solution';
import {DataChart} from '../models/data-chart';
import {Value} from '../models/value';


@Injectable()
export class DecideService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  getDemandTypes(): Observable<DemandType[]> {
    return this.get('demandtypes');
  }

  getModifierTypes(): Observable<ModifierType[]> {
    return this.get('modifiertypes')
      .map((m: ModifierType[]) => m[0] ? m : undefined);
  }



  getIntervalSolutions(): Observable<IntervalSolution[]> {
    return this.get(`intervalsolutions`)
      .map((i: IntervalSolution[]) => i[0] ? i : undefined);
  }

  createIntervalSolution(createIntervalSolution: any): Observable<IntervalSolution> {
    return this.post('intervalsolutions', createIntervalSolution)
      .map((i: IntervalSolution) => i ? i : undefined);
  }


  getDataCharts(): Observable<DataChart[]> {
    return this.get(`datacharts`)
      .map((d: DataChart[]) => d[0] ? d : undefined);
  }

  createDataChart(createDataChart: any): Observable<DataChart> {
    return this.post('datacharts', createDataChart)
      .map((i: DataChart) => i ? i : undefined);
  }


  getValues(): Observable<Value[]> {
    return this.get(`values`)
      .map((v: Value[]) => v[0] ? v : undefined);
  }

  createValue(createValue: any): Observable<Value> {
    return this.post('values', createValue)
      .map((v: Value) => v ? v : undefined);
  }









  deleteIntervalSolution(id: number): Observable<IntervalSolution> {
    return this.delete(`intervalsolutions/${id}`)
      .map((i: IntervalSolution) => i ? i : undefined);
  }

  deleteDataChart(id: number): Observable<DataChart> {
    return this.delete(`datacharts/${id}`)
      .map((d: DataChart) => d ? d : undefined);
  }

  deleteValue(id: number): Observable<Value> {
    return this.delete(`values/${id}`)
      .map((v: Value) => v ? v : undefined);
  }
}


