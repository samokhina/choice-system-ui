import {Component, OnInit} from '@angular/core';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/mergeMap';

import {Chart} from 'chart.js';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';

import {DecideService} from '../../shared/services/decide.service';

import {Dataset, Solution} from '../solutions.component';
import {IntervalSolution} from '../../shared/models/interval-solution';


declare let $: any;


@Component({
    selector: 'app-solution',
    templateUrl: './solution.component.html',
    styleUrls: ['./solution.component.scss']
})

export class SolutionComponent implements OnInit {

    solutions: Solution[] = [];

    datasetsTarget = [];
    datasetsLimitation = [];

    isTarget = false;
    isLimitation = false;

    currentIdSolution;

    constructor(private decideService: DecideService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.getSolutions();
        this.currentIdSolution = +this.route.snapshot.params['id'] - 1;

        window.onafterprint = function () {
            $('.buttons-block').show();
            $('.main-nav-bar').show();
        };
    }


    getSolutions() {
        const user = JSON.parse(window.localStorage.getItem('user'));

        let intervalSolutions$, dataCharts$;

        intervalSolutions$ = this.decideService.getIntervalSolutions()
            .flatMap((intervalSolutions: IntervalSolution[]) => {

                let solutions;
                solutions = intervalSolutions.filter(s => s.user.id === user.id);
                solutions.forEach((s, i) => {
                    this.solutions[i] = new Solution();
                    this.solutions[i].id = s.id;
                    this.solutions[i].formulation = s.formulation;
                    this.solutions[i].result = s.result;
                });
                return this.decideService.getDataCharts();
            });

        dataCharts$ = intervalSolutions$
            .flatMap((data) => {
                let dataCharts;

                this.solutions.forEach((s, i) => {
                    dataCharts = data.filter(d => s.id === d.chart.id);

                    dataCharts.forEach((d, j) => {
                        s.datasets[j] = new Dataset();
                        s.datasets[j].id = d.id;
                        s.datasets[j].label = d.label;
                        s.datasets[j].color = d.color;
                        s.datasets[j].demandType = d.demandType.title;
                        s.datasets[j].modifierType = d.modifierType.title;
                    });
                });

                return this.decideService.getValues();
            });

        dataCharts$
            .subscribe((valuesDataChart) => {
                let values;
                this.solutions.forEach((s, i) => {
                    s.datasets.forEach((d, j) => {
                        values = valuesDataChart.filter(v => v.dataChart.id === d.id);
                        values.forEach((v, k) => {
                            d.valuesX[k] = v.x;
                            if (v.y === -1) {
                                d.valuesY[k] = null;
                            } else {
                                d.valuesY[k] = v.y;
                            }
                        });
                    });
                });

                this.sortValues();

                let dataset;
                let datasets;
                datasets = [];

                const s = this.solutions[this.currentIdSolution];

                s.datasets.forEach((d, i) => {
                    dataset = {
                        label: d.label,
                        data: d.valuesY,
                        backgroundColor: d.color,
                        fill: true
                    };
                    datasets[i] = dataset;
                });

                let dataChart;
                dataChart = {
                    labels: s.datasets[0].valuesX,
                    datasets: datasets
                };

                let optionsChart;
                optionsChart = {
                    legend: {
                        display: false
                    }
                };

                let ctx;
                ctx = document.getElementById('canvas');

                let chart;
                chart = new Chart(ctx, {
                    type: 'line',
                    data: dataChart,
                    options: optionsChart
                });


                s.datasets.forEach(d => {
                    if (d.demandType === 'Ограничение') {
                        this.datasetsLimitation.push(d);
                        this.isLimitation = true;
                    }

                    if (d.demandType === 'Цель') {
                        this.datasetsTarget.push(d);
                        this.isTarget = true;
                    }
                });


                // if (this.datasetsLimitation.length) {
                //   this.datasetsLimitation.forEach((d, i) => {
                //
                //     let dataset = {
                //       label: d.label,
                //       data: d.valuesY,
                //       backgroundColor: d.color,
                //       fill: true
                //     };
                //
                //     datasets = [];
                //     datasets[0] = dataset;
                //
                //     let dataChart = {
                //       labels: s.datasets[0].valuesX,
                //       datasets: datasets
                //     };
                //
                //     let optionsChart = {
                //       legend: {
                //         display: false
                //       }
                //     };
                //
                //     let ctx = document.getElementById('canvas-limitation-' + i);
                //     let chart = new Chart(ctx, {
                //       type: 'line',
                //       data: dataChart,
                //       options: optionsChart
                //     });
                //   });
                // }

                // if (this.datasetsTarget.length) {
                //   this.datasetsTarget.forEach((d, i) => {
                //
                //     dataset = {
                //       label: d.label,
                //       data: d.valuesY,
                //       backgroundColor: d.color,
                //       fill: true
                //     };
                //
                //     datasets = [];
                //     datasets[0] = dataset;
                //
                //     dataChart = {
                //       labels: s.datasets[0].valuesX,
                //       datasets: datasets
                //     };
                //
                //     optionsChart = {
                //       legend: {
                //         display: false
                //       }
                //     };
                //
                //     ctx = document.getElementById('canvas-target-' + i);
                //     chart = new Chart(ctx, {
                //       type: 'line',
                //       data: dataChart,
                //       options: optionsChart
                //     });
                //   });
                // }

            });
    }

    sortValues() {
        this.solutions.forEach((s, i) => {
            s.datasets.forEach((d, j) => {
                const length = d.valuesX.length;

                for (let ik = 0; ik < length - 1; ik++) {
                    for (let jk = length - 1; jk > 0; jk--) {
                        if (d.valuesX[jk] < d.valuesX[jk - 1]) {

                            let k;

                            k = d.valuesX[jk];
                            d.valuesX[jk] = d.valuesX[jk - 1];
                            d.valuesX[jk - 1] = k;

                            k = d.valuesY[jk];
                            d.valuesY[jk] = d.valuesY[jk - 1];
                            d.valuesY[jk - 1] = k;
                        }
                    }
                }
            });
        });
    }

    deleteSolution() {
        let datasets;
        datasets = this.solutions[this.currentIdSolution].datasets;

        let data$;
        data$ = [];
        let deleteValues$;
        deleteValues$ = this.decideService.getValues()
            .flatMap(values => {
                datasets.forEach((d, j) => {
                    let values$;
                    values$ = [];
                    let valuesFilter;
                    valuesFilter = values.filter(v => v.dataChart.id === d.id);
                    valuesFilter.forEach((v, k) => {
                        values$[k] = this.decideService.deleteValue(v.id);
                    });
                    data$[j] = Observable.combineLatest(values$);
                });

                return Observable.combineLatest(data$);
            });

        let deleteDataCharts$;
        deleteDataCharts$ = deleteValues$
            .flatMap(values => {
                data$ = [];
                datasets.forEach((d, j) => {
                    data$[j] = this.decideService.deleteDataChart(d.id);
                });
                return Observable.combineLatest(data$);
            });

        deleteDataCharts$
            .flatMap(deleteData => {
                return this.decideService.deleteIntervalSolution(this.solutions[this.currentIdSolution].id);
            })
            .subscribe(deleteSolution => {
                this.router.navigate(['/system']);
            });
    }

    printSolution() {
        $(document).ready(function () {
            $('.buttons-block').hide();
            $('.main-nav-bar').hide();
            window.print();
        });
    }
}
