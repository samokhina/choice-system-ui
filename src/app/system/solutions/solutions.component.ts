import {Component, OnInit} from '@angular/core';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/mergeMap';

import {Chart} from 'chart.js';

import {User} from '../../shared/models/user';
import {IntervalSolution} from '../shared/models/interval-solution';

import {DecideService} from '../shared/services/decide.service';


declare let $: any;

 export class Solution {
  id: number;

  formulation: string;
  result: number;

  datasets: Dataset[] = [];
}

export class Dataset {
  id: number;

  label: string;
  color: string;

  demandType: string;
  modifierType: string;

  valuesX: number[] = [];
  valuesY: number[] = [];
}

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.scss']
})

export class SolutionsComponent implements OnInit {

  solutions: Solution[] = [];

  constructor(private decideService: DecideService) {
  }

  ngOnInit() {
    ($(document) as any).ready(
      function () {
        const user = JSON.parse(window.localStorage.getItem('user'));
        if (user !== null) {
          $('#arrow-right-button').show();
          $('#side-nav-bar').show();
        }
      });
    this.getSolutions();
  }


  getSolutions() {
    const user = JSON.parse(window.localStorage.getItem('user'));

    let intervalSolutions$, dataCharts$;

    intervalSolutions$ = this.decideService.getIntervalSolutions()
      .flatMap((intervalSolutions: IntervalSolution[]) => {
        let solutions;
        solutions = intervalSolutions.filter(s => s.user.id === user.id);
        solutions.forEach((s, i) => {
          this.solutions[i] = new Solution();
          this.solutions[i].id = s.id;
          this.solutions[i].formulation = s.formulation;
          this.solutions[i].result = s.result;
        });
        return this.decideService.getDataCharts();
      });

    dataCharts$ = intervalSolutions$
      .flatMap((data) => {
        let dataCharts;

        this.solutions.forEach((s, i) => {
          dataCharts = data.filter(d => s.id === d.chart.id);

          dataCharts.forEach((d, j) => {
            s.datasets[j] = new Dataset();
            s.datasets[j].id = d.id;
            s.datasets[j].label = d.label;
            s.datasets[j].color = d.color;
            s.datasets[j].demandType = d.demandType.title;
            s.datasets[j].modifierType = d.modifierType.title;
          });
        });

        return this.decideService.getValues();
      });

    dataCharts$
      .subscribe((valuesDataChart) => {
        let values;
        this.solutions.forEach((s, i) => {
          s.datasets.forEach((d, j) => {
            values = valuesDataChart.filter(v => v.dataChart.id === d.id);
            values.forEach((v, k) => {
              d.valuesX[k] = v.x;
              if (v.y === -1) {
                d.valuesY[k] = null;
              } else {
                d.valuesY[k] = v.y;
              }
            });
          });
        });

        this.sortValues();


        this.solutions.forEach((s, k) => {
          let dataset;
          let datasets;
          datasets = [];

          s.datasets.forEach((d, i) => {
            dataset = {
              label: d.label,
              data: d.valuesY,
              backgroundColor: d.color,
              fill: true
            };
            datasets[i] = dataset;
          });

          const dataChart = {
            labels: s.datasets[0].valuesX,
            datasets: datasets
          };

          const optionsChart = {
            legend: {
              display: false
            }
          };

          const ctx = document.getElementById('canvas-' + k);
          let chart;
          chart = new Chart(ctx, {
            type: 'line',
            data: dataChart,
            options: optionsChart
          });
        });
      });
  }

  sortValues() {
    this.solutions.forEach((s, i) => {
      s.datasets.forEach((d, j) => {
        const length = d.valuesX.length;

        for (let ik = 0; ik < length - 1; ik++) {
          for (let jk = length - 1; jk > 0; jk--) {
            if (d.valuesX[jk] < d.valuesX[jk - 1]) {

              let k;

              k = d.valuesX[jk];
              d.valuesX[jk] = this.solutions[i].datasets[j].valuesX[jk - 1];
              d.valuesX[jk - 1] = k;

              k = d.valuesY[jk];
              d.valuesY[jk] = this.solutions[i].datasets[j].valuesY[jk - 1];
              d.valuesY[jk - 1] = k;
            }
          }
        }
      });
    });
  }
}
