import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SystemComponent} from './system.component';
import {HelpComponent} from './help/help.component';
import {DecideComponent} from './decide/decide.component';
import {SolutionsComponent} from './solutions/solutions.component';
import {SolutionComponent} from './solutions/solution/solution.component';
import {HowToMakeDecisionComponent} from './help/how-to-make-decision/how-to-make-decision.component';
import {HowToManageSolutionsComponent} from './help/how-to-manage-solutions/how-to-manage-solutions.component';
import {HowToViewSolutionsComponent} from './help/how-to-view-solutions/how-to-view-solutions.component';


const routes: Routes = [
  {
    path: 'system', component: SystemComponent, children: [
      {path: '', component: SolutionsComponent},
      {path: 'solution/:id', component: SolutionComponent},
      {path: 'decide', component: DecideComponent},
      {path: 'help', component: HelpComponent},
      {path: 'how-to-make-desion', component: HowToMakeDecisionComponent},
      {path: 'how-to-manage-solutions', component: HowToManageSolutionsComponent},
      {path: 'how-to-view-solutions', component: HowToViewSolutionsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class SystemRoutingModule {

}
