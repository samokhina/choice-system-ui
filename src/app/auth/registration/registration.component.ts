import {Component, OnInit, ViewChild} from '@angular/core';
import {
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import {Router} from '@angular/router';

import {UserService} from '../../shared/services/user.service';
import {User} from '../../shared/models/user';
import {MessageComponent} from '../shared/components/message/message.component';


@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

    form: FormGroup;

    @ViewChild(MessageComponent) message: MessageComponent;

    constructor(private userService: UserService,
                private router: Router) {
    }

    ngOnInit() {

        this.form = new FormGroup({
            'email': new FormControl(
                null,
                [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
            'password': new FormControl(
                null,
                [Validators.required, Validators.minLength(6)]),
            'name': new FormControl(
                null,
                [Validators.required]),
            'agree': new FormControl(
                false,
                [Validators.requiredTrue])
        });
    }

    onSubmit() {
        const {email, password, name} = this.form.value;
        const user = new User(email, password, name);

        if (email !== null && password !== null && name !== null) {
            if (this.form.valid) {
                this.userService.createNewUser(user)
                    .subscribe((u: User) => {
                        this.router.navigate(['/login'], {
                            queryParams: {
                                nowCanLogin: true
                            }
                        });
                    });
            } else {
                if (this.form.value.agree === false) {
                    this.message.showMessage({
                        text: 'Необходимо принять согласие с правилами',
                        type: 'danger'
                    });
                }
                if (this.form.value.agree === true) {
                    this.message.showMessage({
                        text: 'Поля формы регистрации содержат ошибки',
                        type: 'danger'
                    });
                }
            }
        } else {
            this.message.showMessage({
                text: 'Заполните поля формы регистрации',
                type: 'danger'
            });
        }
    }

    // асинхронный валидатор (проверка на сущ. пользователя с введенным emal)
    forbiddenEmails(control: FormControl): Promise<any> {
        return new Promise((resolve, reject) => {
            this.userService.getUserByEmail(control.value)
                .subscribe((user: User) => {
                    if (user) {
                        resolve({
                            forbiddenEmail: true
                        });
                    } else {
                        resolve(null);
                    }
                });
        });
    }
}
