import {
  Component,
  OnInit, ViewChild
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {UserService} from '../../shared/services/user.service';
import {User} from '../../shared/models/user';
import {AuthService} from '../../shared/services/auth.service';
import {MessageComponent} from '../shared/components/message/message.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  @ViewChild(MessageComponent) message: MessageComponent;

  constructor(private userService: UserService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe((params: Params) => {
        if (params['nowCanLogin']) {
          this.message.showMessage({
            text: 'Теперь вы можете зайти в систему',
            type: 'success'
          });
        }
      });

    this.form = new FormGroup({
      'email': new FormControl(
        null,
        [Validators.required, Validators.email]),
      'password': new FormControl(
        null,
        [Validators.required, Validators.minLength(6)])
    });
  }

  onSubmit() {
    const formData = this.form.value;

    if (formData.email !== null && formData.password !== null) {

      this.userService.getUserByEmail(formData.email)
        .subscribe((user: User) => {
          if (user) {
            if (user.password === formData.password) {
              window.localStorage.setItem('user', JSON.stringify(user));
              this.authService.login();
              this.router.navigate(['/system']);
            } else {
              if (formData.password !== null) {
                 this.message.showMessage({
                   text: 'Пароль неверный',
                   type: 'danger'
                 });
              }
            }
          } else {
            this.message.showMessage({
              text: 'Такого пользователя не существует',
              type: 'danger'
            });
          }
        });
    } else {
      this.message.showMessage({
        text: 'Заполните поля формы авторизации',
        type: 'danger'
      });
    }
  }
}
