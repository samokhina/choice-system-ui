import {Component, OnInit} from '@angular/core';
import {Message} from '../../../../shared/models/message';

declare let $: any;

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  message: Message;

  constructor() { }

  ngOnInit() {
  }

  public showMessage(message: Message) {
    this.message = message;
    $('.message-block').css({
      'display': 'block'});

    window.setTimeout(() => {
      $('.message-block').css({
        'display': 'none'});
    }, 5000);
  }
}



