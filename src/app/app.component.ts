import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';

import {User} from './shared/models/user';
import {AuthService} from './shared/services/auth.service';

declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})

export class AppComponent implements OnInit {

  user: User;
  menuState = 'out';

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));

    if (this.user === null) {
      $('#arrow-right-button').hide();
      $('#side-nav-bar').hide();
    } else {
      $('#arrow-right-button').show();
      $('#side-nav-bar').show();
    }

    ($(document) as any).ready(function () {
      ($(window) as any).scroll(
        function () {
          if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
          } else {
            $('#back-to-top').fadeOut();
          }
        });

      ($('#back-to-top')as any).click(function () {
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
      });
    });
  }

  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login']);

    $('#arrow-right-button').hide();
    $('#side-nav-bar').hide();
  }

  hideNavCollapse() {
    // ($('#nav-content')as any).addClass('collapsing');
    ($('#nav-content')as any).removeClass('show');
  }
}
